echo # This script is intended to be sourced.

sh -c "[ `ps $$ | grep bash | wc -l` -gt 0 ] || { echo 'Please switch to the bash shell before running the otsdaq-demo.'; exit; }" || exit

echo -e "setup [275]  \t ======================================================"
echo -e "setup [275]  \t Initially your products path was PRODUCTS=${PRODUCTS}"

unsetup_all

export OTSDAQ_HOME=$PWD
#export PRODUCTS=$OTSDAQ_HOME/products
export PRODUCTS=/cvmfs/fermilab.opensciencegrid.org/products/artdaq/

source ${PRODUCTS}/setup

setup mrb
setup git
source ${OTSDAQ_HOME}/localProducts_otsdaq__s118_e26_prof/setup

#source mrbSetEnv
mrbsetenv
echo -e "setup [275]  \t Now your products path is PRODUCTS=${PRODUCTS}"
echo

# Setup environment when building with MRB (As there's no setupARTDAQOTS file)

export CETPKG_INSTALL=${PRODUCTS}
export CETPKG_J=`nproc`

export OTS_MAIN_PORT=2015

export USER_DATA="${OTSDAQ_HOME}/srcs/otsdaq_template/DataDefault"
export ARTDAQ_DATABASE_URI="filesystemdb://${OTSDAQ_HOME}/srcs/otsdaq_template/databases/filesystemdb/2023_03_Default_db"
export OTSDAQ_DATA="/data/Data"

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo

alias kx='ots -k'

echo
echo -e "setup [275]  \t Now use 'ots -w' to configure otsdaq"
echo -e "setup [275]  \t  	Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t  	Or use 'ots -h' for more options"
echo
echo -e "setup [275]  \t     use 'kx' to kill otsdaq processes"
echo


#setup ninja generator
#============================
ninjaver=`ups list -aKVERSION ninja|sort -V|tail -1|sed 's|"||g'`
setup ninja $ninjaver
alias mb='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J; popd'
alias mbb='mrb b --generator ninja'
alias mz='mrb z; mrbsetenv; mrb b --generator ninja'
alias mt='pushd $MRB_BUILDDIR;ninja -j$CETPKG_J;CTEST_PARALLEL_LEVEL=${CETPKG_J} ninja -j$CETPKG_J test;popd'
alias mtt='mrb t --generator ninja'
alias mi='pushd $MRB_BUILDDIR; ninja -j$CETPKG_J install;popd'
